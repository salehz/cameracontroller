import 'package:custom_splash/custom_splash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'bloc/smsFunction.dart';
import 'cameraLocationList.dart';

Function duringSplash = () {
  //Write your code here
  print('Something background process');
  int a = 123 + 23;
  print(a);

  if (a > 100)
    return 1;
  else
    return 2;
};

Map<int, Widget> op = {1: CircularProgressIndicator(), 2: CircularProgressIndicator()};

void main() => runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SMSFunction()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            fontFamily: 'iransans',
            textTheme: TextTheme(
                headline1: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                headline2: TextStyle(
                    fontSize: 19,
                    fontWeight: FontWeight.w700,
                    color: Colors.black))),
        home: CustomSplash(
          imagePath: 'assets/images/logo.png',
          backGroundColor: Colors.white,
          // backGroundColor: Color(0xfffc6042),
          animationEffect: 'fade-in',
          logoSize: 100,
          home: CameraLocationList(),
          customFunction: duringSplash,
          duration: 2500,
          type: CustomSplashType.StaticDuration,
          outputAndHome: op,
        ),
      ),
    ));
