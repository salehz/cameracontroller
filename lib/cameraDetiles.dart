import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:provider/provider.dart';

import 'bloc/smsFunction.dart';
import 'cameraLists.dart';
import 'model/cameraModel.dart';

class CameraDetile extends StatefulWidget {
  final Camera camera;
  final CameraLocation location;

  CameraDetile({this.camera, this.location});

  @override
  _CameraDetileState createState() => _CameraDetileState();
}

class _CameraDetileState extends State<CameraDetile> {
  String _onTime;
  String _offTime;
  double _intevalTime;
  bool _test1State;
  bool _test2State;
  @override
  void initState() {
    super.initState();

    _onTime = widget.camera.cameraOnTime;
    _offTime = widget.camera.cameraOffTime;
    _test1State = widget.camera.cameraTest1;
    _test2State = widget.camera.cameraTest2;
    _intevalTime = double.parse(widget.camera.cameraIntervalTime);
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<SMSFunction>(context, listen: false).getAllCommand();
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.only(top: 5),
          margin: EdgeInsets.only(top: 40, bottom: 50),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20), color: Colors.white),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      // color: Colors.red,
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 25),
                      height: 40,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[Text("تنظیمات")],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                        alignment: Alignment.topRight,
                        //  color: Colors.green,
                        height: 40,
                        child: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () => Navigator.pop(context))),
                  ),
                ],
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 18, bottom: 10),
                          child: Text(
                            widget.camera.cameraName,
                            overflow: TextOverflow.fade,
                            maxLines: 1,
                            softWrap: false,
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                fontFamily: "tahoma"),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Text(
                widget.location.locationName,
                overflow: TextOverflow.fade,
                maxLines: 2,
                textAlign: TextAlign.center,
                softWrap: false,
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(widget.camera.cameraNumber,
                        style: TextStyle(fontSize: 22, color: Colors.green)),
                    Text(
                      "شماره سیم کارت : ",
                      textDirection: TextDirection.rtl,
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              ),
              Divider(),
              // وضعیت دروبین
              Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    LiteRollingSwitch(
                      //initial value
                      value: _test1State,
                      textOn: 'تست فعال',
                      textOff: 'تست غیرفعال',
                      colorOn: Colors.greenAccent[700],
                      colorOff: Colors.redAccent[700],
                      iconOn: Icons.done,
                      iconOff: Icons.remove_circle_outline,
                      textSize: 12.5,
                      onChanged: (bool state) {
                        //Use it to manage the different states
                        print('Current State of SWITCH IS: $state');
                      },
                      onTap: () {
                        if (_test1State) {
                          context.read<SMSFunction>().sendSMS(
                              widget.camera.cameraNumber,
                              context
                                  .read<SMSFunction>()
                                  .listOfCommands[1]
                                  .command);
                          setState(() {
                            _test1State = !_test1State;
                          });
                        } else {
                          context.read<SMSFunction>().sendSMS(
                              widget.camera.cameraNumber,
                              context
                                  .read<SMSFunction>()
                                  .listOfCommands[0]
                                  .command);
                          setState(() {
                            _test1State = !_test1State;
                          });
                        }
                      },
                    ),
                    RaisedButton(
                      onPressed: () => context.read<SMSFunction>().sendSMS(
                          widget.camera.cameraNumber,
                          context
                              .read<SMSFunction>()
                              .listOfCommands[2]
                              .command),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 17, horizontal: 25),
                        child: Text(
                          "وضعیت",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      color: Colors.blueAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          side: BorderSide(color: Colors.blueAccent)),
                    )
                  ],
                ),
              ),

              Divider(),
              // تنظیمات

              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        _onTime,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(fontFamily: "tahoma", fontSize: 18),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: RaisedButton(
                          color: Colors.green,
                          textColor: Colors.white,
                          onPressed: () => DatePicker.showTimePicker(context,
                                  showSecondsColumn: false,
                                  currentTime: DateTime.now(),
                                  onConfirm: (time) {
                                setState(() {
                                  _onTime = (time.hour.toString() +
                                      ":" +
                                      time.minute.toString());
                                });
                              }),
                          child: Text("زمان روشن شدن")),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text(
                        _offTime,
                        textDirection: TextDirection.ltr,
                        style: TextStyle(fontFamily: "tahoma", fontSize: 18),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: RaisedButton(
                          color: Colors.green,
                          textColor: Colors.white,
                          onPressed: () => DatePicker.showTimePicker(context,
                                  showSecondsColumn: false,
                                  currentTime: DateTime.now(),
                                  onConfirm: (time) {
                                setState(() {
                                  _offTime = (time.hour.toString() +
                                      ":" +
                                      time.minute.toString());
                                });
                              }),
                          child: Text("زمان خاموش شدن")),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 25,
                ),
                child: Column(
                  children: [
                    Slider(
                        value: _intevalTime,
                        activeColor: Colors.green,
                        inactiveColor: Colors.green.shade100,
                        min: 0.0,
                        max: 90.0,
                        divisions: 90,
                        label: _intevalTime.round().toString(),
                        onChanged: (value) {
                          setState(() {
                            _intevalTime = value;
                          });
                        }),
                    Text("مدت وقفه بین عکاسی ${_intevalTime.round()} دقیقه"),
                  ],
                ),
              ),

              /// ذخیره تنظیمات

              InkWell(
                  child: Container(
                    height: 50,
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.green),
                    child: Text("ذخیره تنظیمات دوربین",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        )),
                  ),
                  onTap: () => context
                          .read<SMSFunction>()
                          .updateCamera(Camera(
                              cameraId: widget.camera.cameraId,
                              cameraIntervalTime:
                                  _intevalTime.round().toString(),
                              cameraName: widget.camera.cameraName,
                              cameraNumber: widget.camera.cameraNumber,
                              cameraLocationID:
                                  widget.location.locationId.toString(),
                              cameraOffTime: _offTime,
                              cameraOnTime: _onTime,
                              cameraTest1: _test1State,
                              cameraTest2: _test2State))
                          .whenComplete(() {
                        context
                            .read<SMSFunction>()
                            .sendSMS(widget.camera.cameraNumber,
                                "Time $_onTime $_offTime ${_intevalTime.round().toString()}")
                            .whenComplete(() => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CameraList(
                                          cameraLocation: widget.location,
                                        ))));
                      })),
            ],
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: CircleAvatar(
            maxRadius: 45,
            minRadius: 25,
            backgroundColor: Colors.white,
            backgroundImage: AssetImage('assets/images/camera-lens.png'),
          ),
        ),
      ],
    );
  }
}
