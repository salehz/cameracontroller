import 'dart:convert';

Camera clientFromJson(String str) {
  final jsonData = json.decode(str);
  return Camera.fromMap(jsonData);
}

String clientToJson(Camera data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class CameraLocation {
  int locationId;
  String locationName;

  CameraLocation({this.locationId, this.locationName});

  factory CameraLocation.fromMap(Map<String, dynamic> json) => CameraLocation(
      locationName: json["locationName"], locationId: json["locationID"]);

  Map<String, dynamic> toMap() => {
        "locationName": locationName,
      };
}

class Commands {
  String command;

  Commands({this.command});

  factory Commands.fromMap(Map<String, dynamic> json) =>
      Commands(command: json["command"]);

  Map<String, dynamic> toMap() => {
        "command": command,
      };
}

class Camera {
  int cameraId;
  String cameraName;
  String cameraLocationID;
  String cameraOnTime;
  String cameraOffTime;
  String cameraIntervalTime;
  bool cameraTest1;
  bool cameraTest2;
  String cameraNumber;

  Camera(
      {this.cameraId,
      this.cameraName,
      this.cameraLocationID,
      this.cameraOnTime,
      this.cameraOffTime,
      this.cameraIntervalTime,
      this.cameraTest1,
      this.cameraTest2,
      this.cameraNumber});

  factory Camera.fromMap(Map<String, dynamic> json) => Camera(
      cameraId: json['cameraID'],
      cameraName: json['cameraName'],
      cameraLocationID: json['cameraLocationID'],
      cameraOnTime: json['cameraOnTime'],
      cameraOffTime: json['cameraOffTime'],
      cameraIntervalTime: json['cameraIntervalTime'],
      cameraTest1: json['cameraTest1'] == "true" ? true : false,
      cameraTest2: json['cameraTest2'] == "true" ? true : false,
      cameraNumber: json['cameraNumber']);

  Map<String, dynamic> toMap() => {
        "cameraID": cameraId,
        "cameraName": cameraName,
        "cameraLocationID": cameraLocationID,
        "cameraOnTime": cameraOnTime,
        "cameraOffTime": cameraOffTime,
        "cameraIntervalTime": cameraIntervalTime,
        "cameraTest1": cameraTest1.toString(),
        "cameraTest2": cameraTest2.toString(),
        "cameraNumber": cameraNumber,
      };
}
