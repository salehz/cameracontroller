import 'package:cameracontroller/testBTnSetting.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'addNewCamera.dart';
import 'bloc/smsFunction.dart';
import 'cameraDetiles.dart';
import 'cameraLocationList.dart';
import 'model/cameraModel.dart';

class CameraList extends StatefulWidget {
  final CameraLocation cameraLocation;

  CameraList({this.cameraLocation});

  @override
  _CameraListState createState() => _CameraListState();
}

class _CameraListState extends State<CameraList> {
  @override
  void initState() {
    super.initState();

    Provider.of<SMSFunction>(context, listen: false)
        .getLocationCamera(widget.cameraLocation.locationId.toString());
  }

  @override
  Widget build(BuildContext context) {
    List<Camera> _cameraList = Provider.of<SMSFunction>(
      context,
    ).selectedCameraList;
    double _pagewidth = MediaQuery.of(context).size.width;
    double _pageheight = MediaQuery.of(context).size.height;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            leading: IconButton(
                icon: Icon(
                  Icons.settings,
                  color: Colors.grey,
                ),
                onPressed: () => showTestSetting(context)),
            title: Text(
              "لیست دوربین های ${widget.cameraLocation.locationName}",
              overflow: TextOverflow.fade,
              maxLines: 1,
              softWrap: true,
              style: Theme.of(context).textTheme.headline1,
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            actions: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.arrow_forward,
                    color: Colors.grey,
                  ),
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CameraLocationList()))),
            ],
            elevation: 0.0,
          ),
          body: Container(
            height: _pageheight,
            width: _pagewidth,
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.grey.shade400,
                  height: 0.5,
                  width: _pagewidth * 0.75,
                ),
                Expanded(
                  child: Container(
                      width: _pagewidth,
                      height: _pageheight * 0.80,
                      child: Provider.of<SMSFunction>(context).isloading
                          ? Center(child: CircularProgressIndicator())
                          : _cameraList.length != 0
                              ? ListView.builder(
                                  itemCount: _cameraList.length,
                                  itemBuilder: (context, index) {
                                    return Dismissible(
                                      key: Key(_cameraList[index]
                                          .cameraId
                                          .toString()),
                                      onDismissed: (direction) {
                                        context
                                            .read<SMSFunction>()
                                            .deleteCamera(
                                                _cameraList[index].cameraId)
                                            .whenComplete(() {
                                          setState(() {
                                            _cameraList.removeAt(index);
                                          });
                                        });
                                      },
                                      child: CameraDetiles(
                                          pagewidth: _pagewidth,
                                          cameradetiles: _cameraList[index],
                                          cameraLocation:
                                              widget.cameraLocation),
                                    );
                                  })
                              : Center(
                                  child: Text("هنوز هیچ دوربینی ثبت نشده"))),
                )
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => addNewCamera(context, widget.cameraLocation),
            child: Icon(Icons.add_a_photo, size: 28),
          ),
        ));
  }
}

class CameraDetiles extends StatelessWidget {
  final double pagewidth;
  final Camera cameradetiles;
  final CameraLocation cameraLocation;

  CameraDetiles({this.pagewidth, this.cameradetiles, this.cameraLocation});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
      child: GestureDetector(
        onTap: () => showCameraDetails(context, cameradetiles, cameraLocation),
        child: Container(
          width: pagewidth * 0.51,
          height: 75,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    offset: Offset(0.0, 5.0))
              ]),
          child: Row(
            children: <Widget>[
              Container(
                width: 7,
                height: 75,
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15),
                        bottomRight: Radius.circular(15))),
              ),
              Expanded(
                flex: 3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      cameradetiles.cameraName,
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                    Text(cameradetiles.cameraNumber,
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "tahoma",
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Expanded(
                flex: 3,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.visibility,
                            size: 18,
                            color: Colors.orange,
                          ),
                          Text(
                            " روشــن ",
                            style: TextStyle(fontSize: 12, color: Colors.black),
                          ),
                          Text(
                            cameradetiles.cameraOnTime,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "tahoma",
                                color: Colors.black),
                          )
                        ],
                      ),
                    ),
                    ////
                    Expanded(
                      flex: 4,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.visibility_off,
                            size: 18,
                            color: Colors.blueGrey,
                          ),
                          Text(
                            " خاموش ",
                            style: TextStyle(fontSize: 12, color: Colors.black),
                          ),
                          Text(
                            cameradetiles.cameraOffTime,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "tahoma",
                                color: Colors.black),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 4,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.shutter_speed,
                            size: 18,
                            color: Colors.grey,
                          ),
                          Text(" اینتروال ",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black)),
                          Text(
                            cameradetiles.cameraIntervalTime,
                            textDirection: TextDirection.ltr,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: "tahoma",
                                color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                  flex: 1,
                  child: Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.build,
                          color: cameradetiles.cameraTest1
                              ? Colors.green
                              : Colors.red,
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        cameradetiles.cameraTest1
                            ? Text("فعال",
                                style: TextStyle(
                                    fontSize: 10, color: Colors.green))
                            : Text("غیرفعال",
                                style:
                                    TextStyle(fontSize: 10, color: Colors.red)),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}

showCameraDetails(context, camera, location) {
  return showDialog(
      context: context,
      builder: (BuildContext buildContext) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: CameraDetile(
            camera: camera,
            location: location,
          ),
        );
      });
}

addNewCamera(context, CameraLocation cameraLocation) {
  return showDialog(
      context: context,
      builder: (BuildContext buildContext) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: AddNewCamera(
            cameraLocation: cameraLocation,
          ),
        );
      });
}

showTestSetting(
  context,
) {
  return showDialog(
      context: context,
      builder: (BuildContext buildContext) {
        return Dialog(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
            child: TestBTNSetting());
      });
}
