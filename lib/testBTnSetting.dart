import 'package:cameracontroller/model/cameraModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'bloc/smsFunction.dart';

class TestBTNSetting extends StatefulWidget {
  @override
  _TestBTNSettingState createState() => _TestBTNSettingState();
}

class _TestBTNSettingState extends State<TestBTNSetting> {
  TextEditingController _btn1On = TextEditingController();
  TextEditingController _btn1Off = TextEditingController();
  TextEditingController _btntest = TextEditingController();
  List<Commands> _listOfCommands;

  @override
  Widget build(BuildContext context) {
    Provider.of<SMSFunction>(context, listen: false)
        .getAllCommand()
        .whenComplete(() => {
              _listOfCommands = Provider.of<SMSFunction>(context, listen: false)
                  .listOfCommands,
              if (_listOfCommands.isNotEmpty)
                {
                 
                  _btn1On.text = _listOfCommands[0].command,
                  _btn1Off.text = _listOfCommands[1].command,
                  _btntest.text = _listOfCommands[2].command,
                }
            });
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                      alignment: Alignment.topLeft,
                      //  color: Colors.green,
                      height: 40,
                      child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () => Navigator.pop(context))),
                ),
              ],
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          "تنظیم دستورات دکمه ها",
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          softWrap: false,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: TextField(
                controller: _btn1On,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: "دستور وضعیت فعال",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: TextField(
                controller: _btn1Off,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: "دستور وضعیت غیرفعال",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: TextField(
                controller: _btntest,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: "دستور دکمه تست",
                ),
              ),
            ),
            Divider(),
            // تنظیمات

            InkWell(
                child: Container(
                  height: 50,
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.green),
                  child: Text("ثبت تنظیمات دکمه های تست",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      )),
                ),
                onTap: () => _listOfCommands.isEmpty
                    ? context.read<SMSFunction>().newCommand([
                        Commands(
                          command: _btn1On.text,
                        ),
                        Commands(
                          command: _btn1Off.text,
                        ),
                        Commands(
                          command: _btntest.text,
                        ),
                      ]).whenComplete(() => Navigator.pop(context))
                    : context.read<SMSFunction>().updateCommand([
                        Commands(command: _btn1On.text),
                        Commands(command: _btn1Off.text),
                        Commands(command: _btntest.text),
                      ]).whenComplete(() => Navigator.pop(context)))
          ],
        ),
      ),
    );
  }
}
