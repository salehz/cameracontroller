import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';

import '../model/cameraModel.dart';
import 'package:path/path.dart';
import 'package:flutter/foundation.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sendsms/sendsms.dart';
import 'package:sqflite/sqflite.dart';

class SMSFunction with ChangeNotifier {
  static Database _database;
  bool isloading = true;
  int _cameraCount = 0;

  int get cameraCount {
    return _cameraCount;
  }

  List<CameraLocation> _cameralocationList = [];
  List<Camera> _selectedCameraList = [];
  List<Commands> _listOfCommands = [];

  List<CameraLocation> get cameralocationList {
    return _cameralocationList;
  }

  List<Commands> get listOfCommands {
    return _listOfCommands;
  }

  List<Camera> get selectedCameraList {
    return _selectedCameraList;
  }

  Future<Database> get database async {
    if (_database != null) return _database;

    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "CameraDb.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Camera ("
          "cameraID INTEGER PRIMARY KEY AUTOINCREMENT ,"
          "cameraName TEXT,"
          "cameraLocationID TEXT,"
          "cameraOnTime TEXT,"
          "cameraOffTime TEXT,"
          "cameraIntervalTime TEXT,"
          "cameraTest1 TEXT,"
          "cameraTest2 TEXT,"
          "cameraNumber TEXT"
          ")");

      await db.execute(
          "CREATE TABLE Location (locationID INTEGER PRIMARY KEY AUTOINCREMENT,locationName TEXT );");
      await db.execute(
          "CREATE TABLE testCommand (commandID INTEGER PRIMARY KEY AUTOINCREMENT ,command TEXT );");
    });
  }

  Future<List<CameraLocation>> getAllLocation() async {
    isloading = true;

    final db = await database;
    var res = await db.query("Location");
    _cameralocationList = res.isNotEmpty
        ? res.map((c) => CameraLocation.fromMap(c)).toList()
        : [];
    isloading = false;
    notifyListeners();
    return _cameralocationList;
  }

  newLocation(CameraLocation newLocation) async {
    final db = await database;
    var res = await db.insert("Location", newLocation.toMap());
    return res;
  }

  newCommand(List<Commands> newcommand) async {
    final db = await database;
    newcommand.forEach((element) async {
      await db.insert("testCommand", element.toMap());
    });
    // var res = await db.insert("testCommand", newcommand[0].toMap());

    return null;
  }

  updateCommand(
    List<Commands> command,
  ) async {
    final db = await database;
    await db.update("testCommand", command[0].toMap(), where: "commandID = 1");
    await db.update("testCommand", command[1].toMap(), where: "commandID = 2");
    await db.update("testCommand", command[2].toMap(), where: "commandID = 3");
    // command.forEach((element) async {
    //   await db.update("testCommand", element.toMap(),
    //       where: "commandID = ?", whereArgs: [element]);
    //   print(element.command);
    // });
    return null;
  }

  Future<List<Commands>> getAllCommand() async {
    isloading = true;
    final db = await database;
    var res = await db.query("testCommand");
    _listOfCommands =
        res.isNotEmpty ? res.map((c) => Commands.fromMap(c)).toList() : [];
    isloading = false;
    notifyListeners();
    return _listOfCommands;
  }

  // Future<List<Camera>> getAllCamera() async {
  //   final db = await database;
  //   var res = await db.query("Camera");

  //   List<Camera> list =
  //       res.isNotEmpty ? res.map((c) => Camera.fromMap(c)).toList() : [];
  //   _cameraFindCount = list.length;
  //   notifyListeners();
  //   return list;
  // }

  getLocationCamera(String location) async {
    _cameraCount = 0;
    isloading = true;
    final db = await database;
    var res = await db
        .rawQuery("SELECT * FROM Camera WHERE cameraLocationID=$location");
    _selectedCameraList =
        res.isNotEmpty ? res.map((c) => Camera.fromMap(c)).toList() : [];
    isloading = false;
    _cameraCount = _selectedCameraList.length;
    notifyListeners();
    return _selectedCameraList;
  }

  addNewCamera(Camera newcamera) async {
    final db = await database;
    var res = await db.insert("Camera", newcamera.toMap());
    return res;
  }

  deleteCamera(int id) async {
    final db = await database;
    db.delete("Camera", where: "cameraID = ?", whereArgs: [id]);
  }

  deleteLocation(int id) async {
    final db = await database;
    db.delete("Location", where: "locationID = ?", whereArgs: [id]);
    db.delete("Camera", where: "cameraLocationID = ?", whereArgs: [id]);
  }

  updateCamera(Camera newCamera) async {
    final db = await database;
    var res = await db.update("Camera", newCamera.toMap(),
        where: "cameraID = ?", whereArgs: [newCamera.cameraId]);
    print(res);
    print(newCamera.cameraTest1);
    return res;
  }

  Future sendSMS(String massageNumber, String data) async {
    await Sendsms.onGetPermission();
    if (await Sendsms.hasPermission()) {
      await Sendsms.onSendSMS(massageNumber, data);
    }

    print("Send SMS to $massageNumber massage : $data");
  }
}

//  newLocation(CameraLocation newLocation) async {
//     final db = await database;
//     var res = await db.insert("Location", newLocation.toMap());
//     return res ;
//   }

//   newCamera(Camera newCamera) async {
//     final db = await database;
//     var res = await db.insert("Camera", newCamera.toMap());
//     return res;
//   }

//   getCamera(int id) async {
//     final db = await database;
//     var res = await db.query("Camera", where: "cameraID = ?", whereArgs: [id]);
//     return res.isNotEmpty ? Camera.fromMap(res.first) : Null;
//   }

//   Future<List<Camera>> getAllCamera() async {
//     final db = await database;
//     var res = await db.query("Camera");
//     List<Camera> list =
//         res.isNotEmpty ? res.map((c) => Camera.fromMap(c)).toList() : [];
//     return list;
//   }

//   updateCamera(Camera newCamera) async {
//     final db = await database;
//     var res = await db.update("Camera", newCamera.toMap(),
//         where: "cameraID = ?", whereArgs: [newCamera.cameraId]);
//     return res;
//   }

//   deleteAllCamera() async {
//     final db = await database;
//     db.rawDelete("Delete * from Camera");
//   }
// }

// List<String> settingList = [];
// bool btn01State, btn02State, btn03State;
// bool _isloading = true;

// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   _updateSetting() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     await prefs.setStringList('setting', settingList);
//   }

//   _readSetting() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     List<String> setting = prefs.getStringList('setting') ??
//         [
//           "09154127181",
//           "on",
//           "off",
//           "on",
//           "off",
//           "on",
//           "off",
//           "false",
//           "false",
//           "false"
//         ];
//     print("first Read : $setting");
//     setState(() {
//       settingList = setting;
//       _isloading = false;
//     });
//   }

//   @override
//   void initState() {
//     super.initState();
//     _readSetting();
//   }

//   Future _sendSMS(int massageNumber, int btnNumber, bool newBtnState) async {
//     int msgNmber;
//     print("Run Send SMS in ${settingList[btnNumber]}");
//     print("BTN State is : $newBtnState");
//     await Sendsms.onGetPermission();

//     if (await Sendsms.hasPermission()) {
//       if (newBtnState) {
//         msgNmber = massageNumber;
//       } else {
//         msgNmber = massageNumber + 1;
//       }
//       await Sendsms.onSendSMS(settingList[0], settingList[msgNmber])
//           .whenComplete(
//         () {
//           setState(() {
//             settingList[btnNumber] = newBtnState.toString();
//           });
//         },
//       );
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         centerTitle: true,
//         title: const Text('سیستم کنترلر پیامکی حسن نژاد'),
//         leading: IconButton(
//             icon: Icon(
//               CupertinoIcons.gear,
//               color: Colors.white,
//               size: 35,
//             ),
//             onPressed: () {
//               Navigator.push(
//                   context, MaterialPageRoute(builder: (_) => Setting()));
//             }),
//       ),
//       body: _isloading
//           ? Center(
//               child: CircularProgressIndicator(),
//             )
//           : Container(
//               padding: EdgeInsets.all(15),
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   Text("با انتخاب دکمه های زیر دستگاه خود را کنترل کنید"),
//                   Padding(
//                     padding: const EdgeInsets.only(
//                       top: 25,
//                     ),
//                     child: LiteRollingSwitch(
//                       //initial value
//                       value: settingList[7] == "false" ? false : true,
//                       textOn: 'روشن',
//                       textOff: 'خاموش',
//                       colorOn: Colors.greenAccent[700],
//                       colorOff: Colors.redAccent[700],
//                       iconOn: Icons.done,
//                       iconOff: Icons.remove_circle_outline,
//                       textSize: 16.0,
//                       onChanged: (bool state) {
//                         //Use it to manage the different states
//                         print('Current State of SWITCH IS: $state');

//                         btn01State = state;
//                       },
//                       onTap: () {
//                         if (btn01State.toString() != settingList[7])
//                           _sendSMS(1, 7, btn01State);
//                       },
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(
//                       top: 25,
//                     ),
//                     child: LiteRollingSwitch(
//                       value: settingList[8] == "false" ? false : true,
//                       textOn: 'روشن',
//                       textOff: 'خاموش',
//                       colorOn: Colors.greenAccent[700],
//                       colorOff: Colors.redAccent[700],
//                       iconOn: Icons.done,
//                       iconOff: Icons.remove_circle_outline,
//                       textSize: 16.0,
//                       onChanged: (bool state) {
//                         //Use it to manage the different states
//                         print('Current State of SWITCH IS: $state');

//                         btn02State = state;
//                       },
//                       onTap: () {
//                         if (btn02State.toString() != settingList[8])
//                           _sendSMS(3, 8, btn02State);
//                       },
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(
//                       top: 25,
//                     ),
//                     child: LiteRollingSwitch(
//                       //initial value
//                       value: settingList[9] == "false" ? false : true,
//                       textOn: 'روشن',
//                       textOff: 'خاموش',
//                       colorOn: Colors.greenAccent[700],
//                       colorOff: Colors.redAccent[700],
//                       iconOn: Icons.done,
//                       iconOff: Icons.remove_circle_outline,
//                       textSize: 16.0,
//                       onChanged: (bool state) {
//                         //Use it to manage the different states
//                         print('Current State of SWITCH IS: $state');

//                         btn03State = state;
//                       },
//                       onTap: () {
//                         if (btn03State.toString() != settingList[9])
//                           _sendSMS(5, 9, btn03State);
//                       },
//                     ),
//                   ),
//                 ],
//               )),
//     );
//   }
// }
