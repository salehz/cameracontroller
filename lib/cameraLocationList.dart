import 'package:flutter/material.dart';
import 'addNewLocation.dart';
import 'bloc/smsFunction.dart';
import 'cameraLists.dart';
import 'package:provider/provider.dart';

import 'model/cameraModel.dart';

class CameraLocationList extends StatefulWidget {
  @override
  _CameraLocationListState createState() => _CameraLocationListState();
}

class _CameraLocationListState extends State<CameraLocationList> {
  @override
  void initState() {
    super.initState();
    Provider.of<SMSFunction>(context, listen: false).getAllLocation();
    //  Future.microtask(() => context.read<SMSFunction>().getAllLocation());
  }

  @override
  Widget build(BuildContext context) {
    double _pagewidth = MediaQuery.of(context).size.width;
    double _pageheight = MediaQuery.of(context).size.height;
    List<CameraLocation> _cameraLocationList =
        Provider.of<SMSFunction>(context).cameralocationList;
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text(
              "لیست لوکیشن های دوربین ها",
              style: Theme.of(context).textTheme.headline1,
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0.0,
          ),
          body: Container(
            height: _pageheight,
            width: _pagewidth,
            child: Column(
              children: <Widget>[
                Container(
                  color: Colors.grey.shade400,
                  height: 0.5,
                  width: _pagewidth * 0.75,
                ),
                Expanded(
                  child: Container(
                      width: _pagewidth,
                      height: _pageheight * 0.80,
                      child: Provider.of<SMSFunction>(context).isloading
                          ? Center(child: CircularProgressIndicator())
                          : Provider.of<SMSFunction>(context)
                                      .cameralocationList
                                      .length !=
                                  0
                              ? ListView.builder(
                                  itemCount: _cameraLocationList.length,
                                  itemBuilder: (context, index) {
                                    return Dismissible(
                                      background: Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          color: Colors.red,
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        alignment: Alignment.centerRight,
                                        child: Icon(Icons.delete_forever,
                                            size: 30, color: Colors.white),
                                      ),
                                      key: UniqueKey(),
                                      onDismissed: (direction) {
                                        context
                                            .read<SMSFunction>()
                                            .deleteLocation(
                                                _cameraLocationList[index]
                                                    .locationId)
                                            .whenComplete(() {
                                          setState(() {
                                            _cameraLocationList.removeAt(index);
                                          });
                                        });
                                      },
                                      child: LocationDetiles(
                                        pagewidth: _pagewidth,
                                        cameralocation:
                                            _cameraLocationList[index],
                                      ),
                                    );
                                  })
                              : Center(
                                  child: Text("هنوز هیچ اطلاعاتی ثبت نشده"))),
                )
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => addNewLocation(context),
            child: Icon(Icons.add, size: 30),
          ),
        ));
  }
}

addNewLocation(context) {
  return showDialog(
      context: context,
      builder: (BuildContext buildContext) {
        return Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          child: AddNewLocation(),
        );
      });
}

class LocationDetiles extends StatelessWidget {
  final double pagewidth;
  final CameraLocation cameralocation;

  LocationDetiles({this.cameralocation, this.pagewidth});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
      child: GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CameraList(
                cameraLocation: cameralocation,
              ),
            )),
        child: Container(
          width: pagewidth,
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    offset: Offset(0.0, 5.0))
              ]),
          child: Row(
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Icon(
                      Icons.location_on,
                      color: Colors.deepOrange,
                    ),
                  )),
              Expanded(
                  flex: 8,
                  child: Container(
                    padding: EdgeInsets.only(top: 10, right: 8),
                    child: SizedBox(
                      width: pagewidth * 0.48,
                      height: 25,
                      child: Text(
                        cameralocation.locationName,
                        maxLines: 1,
                        overflow: TextOverflow.fade,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  width: pagewidth * 0.21,
                  height: 25,
                  child: Text(
                    //${context.watch<SMSFunction>().cameraCount}
                      " >    ",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey.shade800)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
