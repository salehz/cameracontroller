import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:provider/provider.dart';

import 'bloc/smsFunction.dart';
import 'cameraLists.dart';
import 'model/cameraModel.dart';

class AddNewCamera extends StatefulWidget {
  CameraLocation cameraLocation;

  AddNewCamera({this.cameraLocation});

  @override
  _AddNewCameraState createState() => _AddNewCameraState();
}

class _AddNewCameraState extends State<AddNewCamera> {
  String _onTime = "00:00";
  String _offTime = "00:00";
  double _intevalTime = 0.0;
  bool _cameraTestState = false;
  TextEditingController _cameraName = TextEditingController();
  TextEditingController _cameraNumber = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Container(
                      alignment: Alignment.topLeft,
                      //  color: Colors.green,
                      height: 40,
                      child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () => Navigator.pop(context))),
                ),
              ],
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          "ثبت دوربین جدید",
                          overflow: TextOverflow.fade,
                          maxLines: 1,
                          softWrap: false,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: TextField(
                controller: _cameraName,
                autofocus: true,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                  hintText: "نام دوربین را وارد کنید",
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: TextField(
                controller: _cameraNumber,
                textAlign: TextAlign.center,
                keyboardType: TextInputType.phone,
                style: TextStyle(fontSize: 20),
                maxLength: 11,
                decoration: InputDecoration(
                    hintText: "شماره سیم کارت دوربین", counterText: ""),
              ),
            ),
            Divider(),
            // تنظیمات

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      _onTime,
                      textDirection: TextDirection.ltr,
                      style: TextStyle(fontFamily: "tahoma", fontSize: 18),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: RaisedButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        onPressed: () => DatePicker.showTimePicker(context,
                                showSecondsColumn: false,
                                currentTime: DateTime.now(), onConfirm: (time) {
                              setState(() {
                                _onTime = (time.hour.toString() +
                                    ":" +
                                    time.minute.toString());
                              });
                            }),
                        child: Text("زمان روشن شدن")),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Text(
                      _offTime,
                      textDirection: TextDirection.ltr,
                      style: TextStyle(fontFamily: "tahoma", fontSize: 18),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: RaisedButton(
                        color: Colors.green,
                        textColor: Colors.white,
                        onPressed: () => DatePicker.showTimePicker(context,
                                showSecondsColumn: false,
                                currentTime: DateTime.now(), onConfirm: (time) {
                              setState(() {
                                _offTime = (time.hour.toString() +
                                    ":" +
                                    time.minute.toString());
                              });
                            }),
                        child: Text("زمان خاموش شدن")),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 25,
              ),
              child: Column(
                children: [
                  Slider(
                      value: _intevalTime,
                      activeColor: Colors.green,
                      inactiveColor: Colors.green.shade100,
                      min: 0.0,
                      max: 90.0,
                      divisions: 90,
                      label: _intevalTime.round().toString(),
                      onChanged: (value) {
                        setState(() {
                          _intevalTime = value;
                        });
                      }),
                  Text("مدت وقفه بین عکاسی ${_intevalTime.round()} دقیقه"),
                ],
              ),
            ),

            /// ذخیره تنظیمات

            InkWell(
              child: Container(
                height: 50,
                alignment: Alignment.center,
                margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.green),
                child: Text("ثبت دوربین جدید",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    )),
              ),
              onTap: () => context
                  .read<SMSFunction>()
                  .addNewCamera(Camera(
                      cameraName: _cameraName.text,
                      cameraIntervalTime: _intevalTime.round().toString(),
                      cameraOffTime: _offTime,
                      cameraOnTime: _onTime,
                      cameraTest1: _cameraTestState,
                      cameraTest2: _cameraTestState,
                      cameraNumber: _cameraNumber.text,
                      cameraLocationID:
                          widget.cameraLocation.locationId.toString()))
                  .whenComplete(() => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CameraList(
                                cameraLocation: widget.cameraLocation,
                              )))),
            ),
          ],
        ),
      ),
    );
  }
}
