import 'package:flutter/material.dart';
import 'bloc/smsFunction.dart';
import 'cameraLocationList.dart';
import 'package:provider/provider.dart';

import 'model/cameraModel.dart';


class AddNewLocation extends StatelessWidget {
  final TextEditingController _locationName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Stack(
        children: [
          Container(
            padding: EdgeInsets.only(top: 5),
            margin: EdgeInsets.only(top: 40, bottom: 50),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                          alignment: Alignment.topLeft,
                          //  color: Colors.green,
                          height: 40,
                          child: IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () => Navigator.pop(context))),
                    ),
                  ],
                ),

                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 18, bottom: 10),
                            child: Text(
                              "ثبت لوکیشن جدید",
                              overflow: TextOverflow.fade,
                              maxLines: 1,
                              softWrap: false,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: TextField(
                    controller: _locationName,
                    decoration: InputDecoration(hintText: "نام لوکیشن"),
                  ),
                ),
                Divider(),

                /// ذخیره تنظیمات

                InkWell(
                    child: Container(
                      height: 50,
                      alignment: Alignment.center,
                      margin: EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.green),
                      child: Text("ذخیره لوکیشن",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          )),
                    ),
                    onTap: () => context
                        .read<SMSFunction>()
                        .newLocation(
                            CameraLocation(locationName: _locationName.text))
                        .whenComplete(
                          () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CameraLocationList())),
                        )),
              ],
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: CircleAvatar(
              maxRadius: 45,
              minRadius: 25,
              backgroundColor: Colors.white,
              child: Icon(
                Icons.location_on,
                size: 50,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
